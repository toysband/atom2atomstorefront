'use strict';

let AbstractMagentoAdapter = require('./abstract');
const util = require('util');
const CacheKeys = require('./cache_keys');
const moment = require('moment')
const _ = require('lodash')
const request = require('request');
const HTTP_RETRIES = 3

/*
 * serial executes Promises sequentially.
 * @param {funcs} An array of funcs that return promises.
 * @example
 * const urls = ['/url1', '/url2', '/url3']
 * serial(urls.map(url => () => $.ajax(url)))
 *     .then(console.log(console))
 */
const serial = funcs =>
  funcs.reduce((promise, func) =>
    promise.then(result => func().then(Array.prototype.concat.bind(result))), Promise.resolve([]))

class ProductAdapter extends AbstractMagentoAdapter {

  constructor(config) {
    super(config);
    this.use_paging = true;
    
    this.stock_sync = true;
    this.custom_sync = true;
    this.media_sync = true;
    this.category_sync = true;
    this.links_sync = true;
    this.configurable_sync = true;
    this.is_federated = true; // by default use federated behaviour
  }

  getEntityType() {
    return 'product';
  }

  getName() {
    return 'adapters/magento/ProductAdapter';
  }

  prepareItems(items) {
    if (!items)
      return null;

    this.total_count = items.total_count;

    if (this.use_paging) {
      this.page_count = Math.round(this.total_count / this.page_size);
      logger.info('Page count', this.page_count)
    }

    return items.items;
  }

  getFilterQuery(context) {
    let query = '';

    if (context.skus) { // pul individual products
      if (!Array.isArray(context.skus))
        context.skus = new Array(context.skus);

      query += 'searchCriteria[filter_groups][0][filters][0][field]=sku&' +
        'searchCriteria[filter_groups][0][filters][0][value]=' + encodeURIComponent(context.skus.join(',')) + '&' +
        'searchCriteria[filter_groups][0][filters][0][condition_type]=in';

    } else if (context.updated_after && typeof context.updated_after == 'object') {
      query += 'searchCriteria[filter_groups][0][filters][0][field]=updated_at&' +
        'searchCriteria[filter_groups][0][filters][0][value]=' + encodeURIComponent(moment(context.updated_after).utc().format()) + '&' +
        'searchCriteria[filter_groups][0][filters][0][condition_type]=gt';
    }
    return query;
  }

  getSourceData(context) {
    const that = this
    const retryHandler = (context, err, reject) => {
      context.retry_count = context.retry_count ? context.retry_count + 1 : 1;
      if (err == null || context.retry_count < HTTP_RETRIES) {
        if (err) {
          logger.error(err);
          logger.info('Retrying getSourceData() request ' + context.retry_count);
        }
        if (this.config.product && this.config.product.synchronizeCatalogSpecialPrices && false) {
          return new Promise((resolve, reject) => {
            this.getProductSourceData(context).then((result) => {
              // download rendered list items
              const products = result.items
              let skus = products.map((p) => { return p.sku })

              if (products.length === 1) { // single product - download child data
                const childSkus = _.flattenDeep(products.map((p) => { return (p.configurable_children) ? p.configurable_children.map((cc) => { return cc.sku }) : null }))
                skus = _.union(skus, childSkus)
              }
              const query = '&searchCriteria[filter_groups][0][filters][0][field]=sku&' +
                'searchCriteria[filter_groups][0][filters][0][value]=' + encodeURIComponent(skus.join(',')) + '&' +
                'searchCriteria[filter_groups][0][filters][0][condition_type]=in';

              this.api.products.renderList(query, this.config.magento.storeId, this.config.magento.currencyCode).then(renderedProducts => {
                context.renderedProducts = renderedProducts
                for (let product of result.items) {
                  const productAdditionalInfo = renderedProducts.items.find(p => p.id === product.id)

                  if (productAdditionalInfo && productAdditionalInfo.price_info) {
                    delete productAdditionalInfo.price_info.formatted_prices
                    delete productAdditionalInfo.price_info.extension_attributes
                    // delete productAdditionalInfo.price_info.special_price
                    product = Object.assign(product, productAdditionalInfo.price_info)

                    if (product.final_price < product.price) {
                      product.special_price = product.final_price
                    }

                    if (this.config.product.renderCatalogRegularPrices) {
                      product.price = product.regular_price
                    }
                  }
                }
                resolve(result)
              })
            }).catch(err => {
              retryHandler(context, err, reject)
            })
          })
        } else {
          return this.getProductSourceData(context).catch(err => {
            retryHandler(context, err, null)
          })
        }
      } else {
        if (reject) {
          reject(err)
        } else {
          throw err
        }
      }
    }

    // run the import logick
    return retryHandler(context, null, null)
  }

  getProductSourceData(context) {
    let query = "";
    let searchCriteria = '&page=%d';
    searchCriteria += "&limit=" + context.page_size;
    //if(this.config.product && JSON.parse(this.config.product.excludeDisabledProducts)) {
    //  searchCriteria += '&searchCriteria[filterGroups][0][filters][0][field]=status'+
    //                    '&searchCriteria[filterGroups][0][filters][0][value]=1';
    //}

    if (typeof context.stock_sync !== 'undefined')
      this.stock_sync = context.stock_sync;

    if (typeof context.category_sync !== 'undefined')
      this.category_sync = context.category_sync;

    if (typeof context.configurable_sync !== 'undefined')
      this.configurable_sync = context.configurable_sync;

    if (context.for_total_count) { // get total counts
      return this.api.products.list(util.format(searchCriteria, 1, 1)).catch((err) => {
        throw new Error(err);
      });
    } else if (context.page && context.page_size) {

      this.use_paging = context.use_paging || false
      this.is_federated = context.use_paging ? false : true;
      this.page = context.page;
      this.page_size = context.page_size

      if (!context.use_paging) this.page_count = 1; // process only one page - used for partitioning purposes

      logger.debug(`Using specific paging options from adapter context: ${context.page} / ${context.page_size}`);

      return this.api.products.list(util.format(searchCriteria, context.page) + (query ? '&' + query : '')).catch((err) => {
        throw new Error(err);
      });

    } else if (this.use_paging) {
      this.is_federated = false; // federated execution is not compliant with paging
      logger.debug(util.format(searchCriteria, this.page) + (query ? '&' + query : ''));
      return this.api.products.list(util.format(searchCriteria, this.page) + (query ? '&' + query : '')).catch((err) => {
        throw new Error(err);
      });
    } else {
      return this.api.products.list().catch((err) => {
        throw new Error(err);
      });
    }
  }

  getTotalCount(context) {
    context = context ? Object.assign(context, { for_total_count: 1 }) : { for_total_count: 1 };
    return this.getSourceData(context); //api.products.list('&searchCriteria[currentPage]=1&searchCriteria[pageSize]=1');
  }

  getLabel(source_item) {
    return `[(${source_item.id} - ${source_item.sku}) ${source_item.name}]`;
  }

  /**
   * 
   * @param {Object} item 
   */
  setFilter(item){
    return new Promise((done, reject) => {
      
    });
  }
  
  
  preProcessItem(item) {


    

        for (let customAttribute of item.custom_attributes) { // map custom attributes directly to document root scope
          item[customAttribute.attribute_code] = customAttribute.value;
        }

        

        item.custom_attributes = null;

        return new Promise((done, reject) => {

          

          // TODO: add denormalization of productcategories into product categories
          // DO NOT use "productcategories" type but rather do search categories with assigned products

          let subSyncPromises = []
          const config = this.config

          // TODO: Refactor the following to "Chain of responsibility"
          // STOCK SYNC
        

          serial(subSyncPromises).then((res) => {

            

            logger.info(`Product sub-stages done for ${item.sku}`)
            return done(item) // all subpromises return refernce to the product
          }).catch(err => {
            logger.error(err);
            return done(item)
          });
        });
  }

  /**
   * We're transorming the data structure of item to be compliant with Smile.fr Elastic Search Suite
   * @param {object} item  document to be updated in elastic search
   */
  normalizeDocumentFormat(item) {
    let prices = new Array();

    /*for (let priceTag of item.tier_prices) {
      prices.push({
        "price": priceTag.value,
        "original_price": priceTag.original_price,
        "customer_group_id": priceTag.customerGroupId,
        "qty": priceTag.qty
      });
    }*/

    if (this.config.vuestorefront && this.config.vuestorefront.invalidateCache) {
      request(this.config.vuestorefront.invalidateCacheUrl + 'P' + item.id, {}, (err, res, body) => {
        if (err) { return console.error(err); }
        console.log(body);
      });
    }

    let resultItem = Object.assign(item, {
      // "price": prices, // ES stores prices differently
      // TODO: HOW TO GET product stock from Magento API call for product?
    });
    return resultItem;
  }
}

module.exports = ProductAdapter;
