'use strict';
const util = require('util');
let AbstractMagentoAdapter = require('./abstract');

class AtomSettingAdapter extends AbstractMagentoAdapter {
    constructor(config) {
        super(config);
        this.use_paging = true;
        this.page_size = 50;
    }

    getEntityType() {
        return 'atom_setting';
    }

    getName() {
        return 'adapters/magento/NewsAdapter';
    }

    getSourceData(context) {
      let query = "";
      let searchCriteria = '&page=%d';
      searchCriteria += "&limit=50";

        this.is_federated = false; // federated execution is not compliant with paging
        logger.debug(util.format(searchCriteria, this.page) + (query ? '&' + query : ''));
        return this.api.atom_setting.list(util.format(searchCriteria, this.page) + (query ? '&' + query : ''))
        // return this.api.atom_setting.list().catch((err) => {
        //     throw new Error(err);
        // });
    }

    prepareItems(items) {
        if(!items)
          return items;

        if (items.total_count)
          this.total_count = items.total_count;

      if (this.use_paging) {
        this.page_count = Math.round(this.total_count / this.page_size);
        logger.info('Page count', this.page_count)
      }

        if (items.items) {
          items = items.items; // this is an exceptional behavior for Magento2 api for lists
        }

        return (items);
    }

    preProcessItem(item) {

        return new Promise((done, reject) => {

            if (item) {
                item.type = 'atom_setting'

            }
            console.debug(item)
          return done(item);
        });

    }

  getTotalCount(context) {
    context = context ? Object.assign(context, { for_total_count: 1 }) : { for_total_count: 1 };
    return this.getSourceData(context); //api.url.list('&searchCriteria[currentPage]=1&searchCriteria[pageSize]=1');
  }

    normalizeDocumentFormat(item) {
        return item;
    }
}

module.exports = AtomSettingAdapter;
