var util = require('util');

module.exports = function (restClient) {
    var module = {};

    module.list = function(searchCriteria) {
        var endpointUrl = util.format('/rest_carts/get_shipping_method_options?'+searchCriteria);
        return restClient.get(endpointUrl);
    }

    return module;
}
