var util = require('util');

module.exports = function (restClient) {
    var module = {};

    module.list = function() {
        return restClient.get('/rest_cms/sections');
    }

    return module;
}
