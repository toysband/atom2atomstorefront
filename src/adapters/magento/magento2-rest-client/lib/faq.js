var util = require('util');

module.exports = function (restClient) {
    var module = {};

    module.list = function (searchCriteria) {

        var endpointUrl = util.format('/rest_cms/get_faq');
        return restClient.get(endpointUrl);
    }

    return module;
}
