var util = require('util');

module.exports = function (restClient) {
    var module = {};

    module.list = function () {
        return restClient.get('/rest_categories');
    }
    
    module.getSingle = function (categoryId) {
        var endpointUrl = util.format('/rest_categories/index/%d', categoryId);
        return restClient.get(endpointUrl);
    }
    
  

    return module;
}
