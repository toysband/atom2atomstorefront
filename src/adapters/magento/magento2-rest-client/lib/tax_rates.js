var util = require('util');

module.exports = function (restClient) {
    var module = {};

    module.list = function () {
        var endpointUrl = util.format('/rest_tax/');
        return restClient.get(endpointUrl);
    }


    return module;
}
