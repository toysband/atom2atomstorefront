'use strict';

let AbstractMagentoAdapter = require('./abstract');

class MainPageProductsAdapter extends AbstractMagentoAdapter {
    constructor(config) {
        super(config);
        this.use_paging = false;
    }

    getEntityType() {
        return 'main_page_products';
    }

    getName() {
        return 'adapters/magento/MainPageProductsAdapter';
    }

    getSourceData(context) {
       

        return this.api.main_page_products.list().catch((err) => {
            throw new Error(err);
        });
    }

    prepareItems(items) {
        if(!items)
          return items;
        let data = [];
        for(var key in items){
            for(var key2 in items[key]){
                let Product = Object.assign({}, items[key][key2]['Product'])
                Product.main_type = key;
                //Product.sku = Product.id+""
                //Product.id = Product.id
                //Product.tsk = this.getCurrentContext().transaction_key;
                if(Product.name){
                    data.push(Product);
                }
                
            }
        }
        logger.debug(data)
        return (data);
    }

    preProcessItem(item) {

        return new Promise((done, reject) => {
            if (item) {
                item.type = 'main_page_products'
            }
         
          return done(item);
        });

    }

    normalizeDocumentFormat(item) {
        return item;
    }
}

module.exports = MainPageProductsAdapter;
