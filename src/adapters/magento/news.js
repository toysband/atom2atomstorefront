'use strict';

let AbstractMagentoAdapter = require('./abstract');

class NewsAdapter extends AbstractMagentoAdapter {
    constructor(config) {
        super(config);
        this.use_paging = false;
    }

    getEntityType() {
        return 'news';
    }

    getName() {
        return 'adapters/magento/NewsAdapter';
    }

    getSourceData(context) {
       

        return this.api.news.list().catch((err) => {
            throw new Error(err);
        });
    }

    prepareItems(items) {
        if(!items)
          return items;

        if (items.total_count)
          this.total_count = items.total_count;

        if (items.items) {
          items = items.items; // this is an exceptional behavior for Magento2 api for lists
        }
    
        return (items);
    }

    preProcessItem(item) {

        return new Promise((done, reject) => {
            if (item) {
                item.type = 'news'
            }
         
          return done(item);
        });

    }

    normalizeDocumentFormat(item) {
        return item;
    }
}

module.exports = NewsAdapter;
