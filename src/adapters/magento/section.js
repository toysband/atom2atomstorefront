'use strict';

let AbstractMagentoAdapter = require('./abstract');

class SectionAdapter extends AbstractMagentoAdapter {
    constructor(config) {
        super(config);
        this.use_paging = false;
    }

    getEntityType() {
        return 'section';
    }

    getName() {
        return 'adapters/magento/SectionAdapter';
    }

    getSourceData(context) {
       

        return this.api.section.list().catch((err) => {
            throw new Error(err);
        });
    }

    prepareItems(items) {
        if(!items)
          return items;

        if (items.total_count)
          this.total_count = items.total_count;

        if (items.items) {
          items = items.items; // this is an exceptional behavior for Magento2 api for lists
        }
    
        return (items);
    }

    preProcessItem(item) {

        return new Promise((done, reject) => {
            if (item) {
                item.type = 'section'
            }
         
          return done(item);
        });

    }

    normalizeDocumentFormat(item) {
        return item;
    }
}

module.exports = SectionAdapter;
