'use strict';

let AbstractMagentoAdapter = require('atom2atomstorefront/src/adapters/magento/abstract');
const util = require('util');
const CacheKeys = require('atom2atomstorefront/src/adapters/magento/cache_keys');
const moment = require('moment')
const _ = require('lodash')
const request = require('request');
const HTTP_RETRIES = 3

/*
 * serial executes Promises sequentially.
 * @param {funcs} An array of funcs that return promises.
 * @example
 * const urls = ['/url1', '/url2', '/url3']
 * serial(url.map(url => () => $.ajax(url)))
 *     .then(console.log(console))
 */
const serial = funcs =>
  funcs.reduce((promise, func) =>
    promise.then(result => func().then(Array.prototype.concat.bind(result))), Promise.resolve([]))

class UrlAdapter extends AbstractMagentoAdapter {

  constructor(config) {
    super(config);
    this.use_paging = true;
    this.page_size = 300;
    this.stock_sync = false;
    this.custom_sync = false;
    this.media_sync = false;
    this.category_sync = false;
    this.links_sync = false;
    this.configurable_sync = false;
    this.is_federated = false; // by default use federated behaviour
  }

  getEntityType() {
    return 'shipping_options';
  }

  getName() {
    return 'adapters/magento/UrlAdapter';
  }

  prepareItems(items) {
    if (!items)
      return null;

    this.total_count = items.total_count;

    if (this.use_paging) {
      this.page_count = Math.round(this.total_count / this.page_size);
      logger.info('Page count', this.page_count)
    }

    return items.items;
  }

  getFilterQuery(context) {
    let query = '';

    
    return query;
  }

  getSourceData(context) {
    const that = this
    const retryHandler = (context, err, reject) => {
      context.retry_count = context.retry_count ? context.retry_count + 1 : 1;
      if (err == null || context.retry_count < HTTP_RETRIES) {
        if (err) {
          logger.error(err);
          logger.info('Retrying getSourceData() request ' + context.retry_count);
        }
        
        return this.geturlSourceData(context).catch(err => {
          retryHandler(context, err, null)
        })
      } else {
        if (reject) {
          reject(err)
        } else {
          throw err
        }
      }
    }


    // run the import logick
    return retryHandler(context, null, null)
    
  }

  geturlSourceData(context) {
    let query = "";
    let searchCriteria = '&page=%d';
    searchCriteria += "&limit=" + context.page_size;
    //if(this.config.url && JSON.parse(this.config.shipping_options.excludeDisabledurls)) {
    //  searchCriteria += '&searchCriteria[filterGroups][0][filters][0][field]=status'+
    //                    '&searchCriteria[filterGroups][0][filters][0][value]=1';
    //}


    if (context.for_total_count) { // get total counts
      return this.api.shipping_option.list(util.format(searchCriteria, 1, 1)).catch((err) => {
        throw new Error(err);
      });
    } else if (context.page && context.page_size) {

      this.use_paging = context.use_paging || false
      this.is_federated = context.use_paging ? false : true;
      this.page = context.page;
      this.page_size = context.page_size

      if (!context.use_paging) this.page_count = 1; // process only one page - used for partitioning purposes

      logger.debug(`Using specific paging options from adapter context: ${context.page} / ${context.page_size}`);

      return this.api.shipping_options.list(util.format(searchCriteria, context.page) + (query ? '&' + query : '')).catch((err) => {
        throw new Error(err);
      });

    } else if (this.use_paging) {
      this.is_federated = false; // federated execution is not compliant with paging
      logger.debug(util.format(searchCriteria, this.page) + (query ? '&' + query : ''));
      return this.api.shipping_options.list(util.format(searchCriteria, this.page) + (query ? '&' + query : '')).catch((err) => {
        throw new Error(err);
      });
    } else {
      return this.api.shipping_options.list().catch((err) => {
        throw new Error(err);
      });
    }
  }

  getTotalCount(context) {
    context = context ? Object.assign(context, { for_total_count: 1 }) : { for_total_count: 1 };
    return this.getSourceData(context); //api.shipping_options.list('&searchCriteria[currentPage]=1&searchCriteria[pageSize]=1');
  }

  getLabel(source_item) {
    return `[(${source_item.id} - ${source_item.id}) ${source_item.name}]`;
  }

  /**
   * 
   * @param {Object} item 
   */
  setFilter(item){
    return new Promise((done, reject) => {
      
    });
  }
  
  
  preProcessItem(item) {


    


        

        return new Promise((done, reject) => {

          

          // TODO: add denormalization of urlcategories into url categories
          // DO NOT use "urlcategories" type but rather do search categories with assigned urls

          let subSyncPromises = []
          const config = this.config

          // TODO: Refactor the following to "Chain of responsibility"
          // STOCK SYNC
        

          serial(subSyncPromises).then((res) => {

            item.type = 'shipping_options'

            //logger.info(`url sub-stages done for ${item.sku}`)
            return done(item) // all subpromises return refernce to the url
          }).catch(err => {
            logger.error(err);
            return done(item)
          });
        });
  }

  /**
   * We're transorming the data structure of item to be compliant with Smile.fr Elastic Search Suite
   * @param {object} item  document to be updated in elastic search
   */
  normalizeDocumentFormat(item) {
    let prices = new Array();

    /*for (let priceTag of item.tier_prices) {
      prices.push({
        "price": priceTag.value,
        "original_price": priceTag.original_price,
        "customer_group_id": priceTag.customerGroupId,
        "qty": priceTag.qty
      });
    }*/

    if (this.config.vuestorefront && this.config.vuestorefront.invalidateCache) {
      request(this.config.vuestorefront.invalidateCacheUrl + 'P' + item.id, {}, (err, res, body) => {
        if (err) { return console.error(err); }
        console.log(body);
      });
    }

    let resultItem = Object.assign(item, {
      // "price": prices, // ES stores prices differently
      // TODO: HOW TO GET url stock from Magento API call for url?
    });
    return resultItem;
  }
}

module.exports = UrlAdapter;
