'use strict';

let AbstractMagentoAdapter = require('./abstract');

class TaxruleAdapter extends AbstractMagentoAdapter {

  getEntityType() {
    return 'taxrule';
  }

  getName() {
    return 'adapters/magento/TaxrulesAdapter';
  }

  getSourceData(context) {
    return this.api.taxRates.list();
  }

  getLabel(source_item) {
    return `[(${source_item.id}) ${source_item.code}]`;
  }

  isFederated() {
    return false;
  }

  preProcessItem(item) {
    return new Promise((done, reject) => {

      return done(item);
    });
  }

  prepareItems(items) {
    if(!items)
      return null;

    return items;
  }
  
  /**
   * We're transorming the data structure of item to be compliant with Smile.fr Elastic Search Suite
   * @param {object} item  document to be updated in elastic search
   */
  normalizeDocumentFormat(item) {
    return item;
  }
}

module.exports = TaxruleAdapter;
